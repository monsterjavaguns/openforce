# The Open Force: Heroes of the Code

![Open Force: Heroes of the Code lead image](osdc-superhero.jpg)

April 28th is National Superhero Day! Of course, that led a few of us here at Opensource.com to wonder what some of our favorite open source tools and mascots would be like if they were superheroes. Granted, open source tools are already kind of like superpowers in their own right, but we didn't let that stop us! So I present you with the *Open Force, Heroes of the Code*! (with accompanying illustration, of course)

## SuperTux

![SuperTux](supertux.png)

By day, he's your standard tuxedo-wearing penguin. By night, he's still a penguin. However, he dons the cape and mask because it suits his style—he makes it look good. Fast and reliable, SuperTux only looks like he's sitting idle when in reality he's everywhere he needs to be, even *here* for *you*.

## Captain Blender

![Captain Blender](cptblender.png)

Both powerful and graceful, there's no puzzle she can't solve. With mastery of all dimensions of time and space, she outmatches every opponent. She's an excellent ally, especially once you understand how she thinks.

## WereGIMP

![WereGIMP](weregimp.png)

Wilber looks like your typical mild-mannered artist, but when the need arrives he can "beast out" and wrestle whatever image manipulation tasks you put in front of him.

## Big Momma Mozilla

![Big Momma Mozilla](mommozilla.png)

The big red dragonosaur is the connective glue of the team. Normally more docile than depicted, she provides the team with the tools they need to communicate with each other and the rest of the world. But woe to any baddie who tries to discover any of her teammates' secret identities! Those teeth aren't just for show.

## The VideoLANner

![The VideoLANner](videolanner.png)

Don't let the cone mask fool you. With his handy remote, the VideoLANner knows no restrictions. He can play, stop, and record any situation with one hand behind his back.

## MechaBSD Daemon

![MechaBSD Daemon](mechabsd.png)

He's capable enough on his own, but when paired with his mechanical suit, the BSD Daemon is unstoppable. He specializes in having the exact right tool for every job, whether it's saving the world or saving a cat.

What other members of the Open Force would you add? An Emacs utility belt? The Invisible Vim? X-Terminator? Share your ideas down in the comments... or even better, sketch out what they look like and add them to the team yourself! There's a high resolution version of the lead image attached to this article, but the source file is a Krita file and we've put it up in [a repository on GitLab](https://gitlab.com/monsterjavaguns/openforce). Have at it!
