# openforce art repository

![Open Force: Heroes of the Code lead image](osdc-superhero.jpg)

## The Open Force: Heroes of the Code

Source images and text for [a fun article on Opensource.com](https://opensource.com/article/19/4/open-force-heroes-code) for National Superhero Day.

Everything in this repository is available under the [Creative Commons Attribution-ShareAlike 4.0 International license](https://creativecommons.org/licenses/by-sa/4.0/). Pull down the (admittedly large) Krita source image and add your own heroes to the mix! Descriptions of superpowers encouraged!

(Fair warning: the Krita file is high resolution and currently weighs in at about 70MB. It might take a second to download.)
